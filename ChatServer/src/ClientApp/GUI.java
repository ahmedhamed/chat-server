package ClientApp;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import ChatServer.*;

public class GUI extends JFrame implements ActionListener {

	public static final long serialVersionUID = 1L;
	public JPanel panel;
	public AbstractUser userLogIn;
	public SignView signView;
	public RoomView roomView;
	public ChatServer chatServer;
	public boolean signUpCliked;
	public List<ChatMessage> messages;

	GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		defineMainPanel();
		chatServer = new ChatServer().getInstance();
		chatServer.loadPersistanceConfigurations();
		messages = new ArrayList<ChatMessage>();
		roomView = new RoomView(this, panel);
		signView = new SignView(this, panel);
		executeSignInUpButton();
	}

	public void defineMainPanel() {
		panel = new JPanel();
		this.add(panel);
		panel.setLayout(null);
		panel.setVisible(true);
	}

	public boolean userExit(String userName, String passWord) {
		List<IUser> users = chatServer.getUsers();
		for (int i = 0; i < users.size(); ++i) {
			IUser cur = users.get(i);
			if (cur.getName().equals(userName)
					&& cur.getPassWord().equals(passWord)) {
				userLogIn = (AbstractUser) cur;
				return true;
			}
		}
		return false;
	}

	public void executeSignInUpButton() {
		signView.setAllVisiable(1);
		roomView.setAllVisiable(0);
	}

	public void executeSignInInput() {
		signView.setAllVisiable(2);
		roomView.setAllVisiable(0);
	}

	public void executeSignUpInput() {
		signView.setAllVisiable(3);
		roomView.setAllVisiable(0);
	}

	public void executeRoomList(String userName) {
		roomView.UpdateRoomList(chatServer.getRooms(), userLogIn);
		signView.setAllVisiable(4);
		if (userLogIn instanceof GeneralUser)
			roomView.setAllVisiable(1);
		else
			roomView.setAllVisiable(2);
	}

	public void executeConfirm() {
		String userName = signView.getUserName();
		String passWord = signView.getPassWord();
		String confirmPassWord = signView.getConfirmPassWord();
		userLogIn = new GeneralUser();
		userLogIn.setName(userName);
		userLogIn.setPassWord(passWord);
		if (signUpCliked) {
			if (passWord.equals(confirmPassWord)) {
				chatServer.addUser((GeneralUser) userLogIn);
				if (!userExit(userName, passWord))
					signView.warningMessage("User Name Already Exit.");
				else
					executeRoomList(userName);
			} else
				signView.warningMessage("Wrong User Name Or PassWord.");
		} else if (!userExit(userName, passWord))
			signView.warningMessage("Wrong User Name Or PassWord.");
		else
			executeRoomList(userName);
		signView.setAllEmpty();
	}

	public void executeAskJoin() {
		IRoom room = roomView.getSelectedRoom();
		if (room == null)
			return;
		if (room.getDesciption().equals("general")) {
			chatServer.joinRoom(room.getID(), userLogIn.getID());
			signView.warningMessage("You are join to this room successfully.");
		} else {
			RoomMessage message = new RoomMessage(userLogIn, room,
					"Ask to join this room.");
			messages.add(message);
			signView.warningMessage("Request Sent.");
		}
	}

	public void executeLeaving() {
		IRoom room = roomView.getSelectedUserRoom();
		if (room == null)
			return;
		if (room.getDesciption().equals("general"))
			chatServer.leaveRoom(room.getID(), userLogIn.getID());
		else {
			IUser roomOwner = room.getUsers().get(0);
			if (roomOwner.getName().equals(userLogIn.getName())) {
				signView.warningMessage("This Room will deleted Because You Are Owner Of This Room.");
				chatServer.removeRoom(room.getID());
			} else if (userLogIn instanceof AdminUser)
				chatServer.removeRoom(room.getID());
			else {
				IUser sender = new AbstractUser();
				sender.setName(room.getTitle());
				RoomMessage leavingMessage = new RoomMessage(sender, room,
						userLogIn.getName() + " Leaving Room.");
				messages.add(leavingMessage);
				chatServer.leaveRoom(room.getID(), userLogIn.getID());
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void executeOpenRoom(boolean type) {
		AbstractRoom room;
		if (type)
			room = (AbstractRoom) roomView.getSelectedRoom();
		else
			room = (AbstractRoom) roomView.getSelectedUserRoom();
		if (room == null)
			return;
		if (room.getDesciption().equals("general") || !type) {
			JFrame frame = new JFrame();
			frame.setSize(700, 300);
			frame.show();
			JLabel[] messagesLabel = new JLabel[messages.size()];
			int index = 0;
			for (int i = 0; i < messages.size(); ++i) {
				if (messages.get(i) instanceof RoomMessage) {
					RoomMessage roomMessage = (RoomMessage) messages.get(i);
					for (int j = 0; j < roomMessage.roomList.size(); ++j) {
						if (roomMessage.roomList.get(j).getID() == room.getID()) {
							String message = "";
							messagesLabel[index] = new JLabel();
							messagesLabel[index].setBounds(0, (20 * index),
									700, 20);
							message += roomMessage.getSender().getName();
							message += ": ";
							message += roomMessage.getMessage();
							messagesLabel[index].setText(message);
							frame.add(messagesLabel[index++]);
						}
					}
				}
			}
		} else
			signView.warningMessage("This Is Closed Room.");
	}

	public void executeAddUser() {
		IRoom room = roomView.getSelectedUserRoom();
		if (room.getUsers().get(0).getID() != userLogIn.getID())
			signView.warningMessage("You Aren't Owner Of This Room.");
		else {
			String userName = (String) JOptionPane.showInputDialog(null,
					"Enter User Name", "", JOptionPane.PLAIN_MESSAGE);
			boolean exit = false;
			List<IUser> users = chatServer.getUsers();
			for (int i = 0; i < users.size(); ++i) {
				if (users.get(i).getName().equals(userName)) {
					exit = true;
					room.addUser(users.get(i));
					chatServer.joinRoom(room.getID(), users.get(i).getID());
				}
			}
			if (!exit)
				signView.warningMessage("User Name Doesn't Exit.");
			else {
				IUser sender = new AbstractUser();
				sender.setName(room.getTitle());
				RoomMessage message = new RoomMessage(sender, room, userName
						+ " join this room.");
				messages.add(message);
			}
		}
	}

	public void executeSendMessageUser(boolean type) {
		List<IUser> users = new ArrayList<IUser>();
		boolean anyOne = false;
		while (true) {
			String userName = (String) JOptionPane.showInputDialog(null,
					"Enter User Name", "", JOptionPane.PLAIN_MESSAGE);
			if (userName == null)
				break;
			boolean exit = false, blocked = false;
			for (int i = 0; i < chatServer.getUsers().size(); ++i) {
				IUser user = chatServer.getUsers().get(i);
				if (user.getName().equals(userName)) {
					if (user instanceof GeneralUser) {
						GeneralUser thisUser = (GeneralUser) user;
						List<IUser> blockedUsers = thisUser.getBlockedUsers();
						for (int j = 0; j < blockedUsers.size(); ++j) {
							if (blockedUsers.get(j).getID() == userLogIn
									.getID()) {
								blocked = true;
								break;
							}
						}
					}
					if (blocked)
						break;
					users.add(user);
					exit = true;
				}
			}
			if (blocked)
				signView.warningMessage("You Can't Send Message To This User.");
			else if (!exit)
				signView.warningMessage("User Name Doesn't Exit.");
			else {
				anyOne = true;
				if (type)
					break;
			}
		}
		if (anyOne) {
			String message = (String) JOptionPane.showInputDialog(null,
					"Enter Message", "", JOptionPane.PLAIN_MESSAGE);
			UserMessage newMessage;
			if (type)
				newMessage = new UserMessage(userLogIn, users.get(0), message);
			else
				newMessage = new UserMessage(userLogIn, users, message);
			messages.add(newMessage);
			signView.warningMessage("Message Sent Successfully.");
		}
	}

	public void executeSendMessageRoom(boolean type) {
		List<IRoom> rooms = new ArrayList<IRoom>();
		boolean anyOne = false;
		while (true) {
			String roomName = (String) JOptionPane.showInputDialog(null,
					"Enter Room Title", "", JOptionPane.PLAIN_MESSAGE);
			if (roomName == null)
				break;
			boolean exit = false;
			for (int i = 0; i < chatServer.getRooms().size(); ++i) {
				IRoom room = chatServer.getRooms().get(i);
				if (room.getTitle().equals(roomName)) {
					rooms.add(room);
					exit = true;
				}
			}
			if (!exit)
				signView.warningMessage("Room Title Doesn't Exit.");
			else {
				anyOne = true;
				if (type)
					break;
			}
		}
		if (anyOne) {
			String message = (String) JOptionPane.showInputDialog(null,
					"Enter Message", "", JOptionPane.PLAIN_MESSAGE);
			RoomMessage newMessage;
			if (type)
				newMessage = new RoomMessage(userLogIn, rooms.get(0), message);
			else
				newMessage = new RoomMessage(userLogIn, rooms, message);
			messages.add(newMessage);
			signView.warningMessage("Message Sent Successfully.");
		}
	}

	public void executeSendMessage() {
		String output = "Enter For Who You Will Send This Message (User, Users, Room, Rooms)";
		String choose = (String) JOptionPane.showInputDialog(null, output, "",
				JOptionPane.PLAIN_MESSAGE);
		if (choose == null)
			return;
		else if (choose.equals("User"))
			executeSendMessageUser(true);
		else if (choose.equals("Users"))
			executeSendMessageUser(false);
		else if (choose.equals("Room"))
			executeSendMessageRoom(true);
		else if (choose.equals("Rooms"))
			executeSendMessageRoom(false);
		else
			executeSendMessage();
	}

	@SuppressWarnings("deprecation")
	public void executeShowMessage() {
		JFrame frame = new JFrame();
		frame.setSize(700, 300);
		frame.show();
		JLabel[] messagesLabel = new JLabel[messages.size()];
		int index = 0;
		for (int i = 0; i < messages.size(); ++i) {
			UserMessage userMessage = (UserMessage) messages.get(i);
			if (userMessage == null)
				continue;
			for (int j = 0; j < userMessage.recipients.size(); ++j) {
				if (userMessage.recipients.get(j).getID() == userLogIn.getID()) {
					String message = "";
					messagesLabel[index] = new JLabel();
					messagesLabel[index].setBounds(0, (20 * index), 700, 20);
					message += userMessage.getSender().getName();
					message += ": ";
					message += userMessage.getMessage();
					messagesLabel[index].setText(message);
					frame.add(messagesLabel[index++]);
				}
			}
		}
	}

	public void executeCreateRoom() {
		String roomName = (String) JOptionPane.showInputDialog(null,
				"Enter Room Title", "", JOptionPane.PLAIN_MESSAGE);
		for (int i = 0; i < chatServer.getRooms().size(); ++i) {
			if (chatServer.getRooms().get(i).getTitle().equals(roomName)) {
				signView.warningMessage("Room Title Already Exit.");
				return;
			}
		}
		String roomType = (String) JOptionPane.showInputDialog(null,
				"Enter Room Type(General, Closed)", "",
				JOptionPane.PLAIN_MESSAGE);
		if (roomType.equals("General"))
			chatServer.createGenralRoom(roomName, roomType, userLogIn);
		else if (roomType.equals("Closed")) {
			List<IUser> allowdUsers = new ArrayList<IUser>();
			allowdUsers.add(userLogIn);
			chatServer.creatRestirctedRoom(roomName, roomType, allowdUsers);
		}
	}

	public void executeBlockUser() {
		String userName = (String) JOptionPane.showInputDialog(null,
				"Enter User Name", "", JOptionPane.PLAIN_MESSAGE);
		if (userName.equals(userLogIn.getName()))
			signView.warningMessage("You Can't Block Youself.");
		boolean exit = false;
		for (int i = 0; i < chatServer.getUsers().size(); ++i) {
			if (chatServer.getUsers().get(i).getName().equals(userName)) {
				chatServer.blockUser(userLogIn.getID(), chatServer.getUsers()
						.get(i).getID());
				exit = true;
			}
		}
		if (!exit)
			signView.warningMessage("User Name Doesn't Exit.");
	}

	public void executeRemoveUser() {
		String userName = (String) JOptionPane.showInputDialog(null,
				"Enter User Name", "", JOptionPane.PLAIN_MESSAGE);
		if (userName.equals(userLogIn.getName()))
			signView.warningMessage("You Can't Block Youself.");
		boolean exit = false;
		for (int i = 0; i < chatServer.getUsers().size(); ++i) {
			if (chatServer.getUsers().get(i).getName().equals(userName)) {
				chatServer.removeUser(chatServer.getUsers().get(i).getID());
				exit = true;
			}
		}
		if (!exit)
			signView.warningMessage("User Name Doesn't Exit.");
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if (command.equals("signIn")) {
			signUpCliked = false;
			executeSignInInput();
		} else if (command.equals("signUp")) {
			signUpCliked = true;
			executeSignUpInput();
		} else if (command.equals("back") || command.equals("logout"))
			executeSignInUpButton();
		else if (command.equals("confirm"))
			executeConfirm();
		else if (command.equals("askjoin")) {
			executeAskJoin();
			roomView.setAllVisiable(0);
			executeRoomList(userLogIn.getName());
		} else if (command.equals("openroom"))
			executeOpenRoom(true);
		else if (command.equals("openuserroom"))
			executeOpenRoom(false);
		else if (command.equals("removeroom")) {
			executeLeaving();
			roomView.setAllVisiable(0);
			executeRoomList(userLogIn.getName());
		} else if (command.equals("adduser"))
			executeAddUser();
		else if (command.equals("sendmessage"))
			executeSendMessage();
		else if (command.equals("showmessage"))
			executeShowMessage();
		else if (command.equals("createroom")) {
			executeCreateRoom();
			roomView.setAllVisiable(0);
			executeRoomList(userLogIn.getName());
		} else if (command.equals("blockuser"))
			executeBlockUser();
		else if (command.equals("removeuser"))
			executeRemoveUser();
	}
}
package ClientApp;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class SignView {
	public JFrame frame;
	public JPanel panel;
	public JButton signIn, signUp, logOut, back, confirm;
	public JTextField userName;
	public JPasswordField passWord, confirmPassWord;
	public JLabel userNameLabel, passWordLabel, confirmPassWordLabel;

	public SignView(JFrame mainFrame, JPanel mainPanel) {
		frame = mainFrame;
		panel = mainPanel;

		defineSignInButton();
		defineSignUpButton();
		defineBackButton();
		defineConfirmButton();
		defineLogOutButton();

		defineUserNameInputField();
		definePassWordInputField();
		defineConfirmPassWordInputField();

		defineUserNameLabelField();
		definePassWordLabelField();
		defineConfirmPassWordLabelField();
	}

	public void defineSignInButton() {
		signIn = new JButton("Sign In");
		signIn.setBounds(300, 300, 150, 30);
		signIn.setActionCommand("signIn");
		signIn.addActionListener((ActionListener) frame);
		signIn.setContentAreaFilled(true);
		panel.add(signIn);
	}

	public void defineSignUpButton() {
		signUp = new JButton("Sign Up");
		signUp.setBounds(300, 340, 150, 30);
		signUp.setActionCommand("signUp");
		signUp.addActionListener((ActionListener) frame);
		signUp.setContentAreaFilled(true);
		panel.add(signUp);
	}

	public void defineBackButton() {
		back = new JButton("Back");
		back.setBounds(533, 650, 150, 30);
		back.setActionCommand("back");
		back.addActionListener((ActionListener) frame);
		back.setContentAreaFilled(true);
		panel.add(back);
	}

	public void defineConfirmButton() {
		confirm = new JButton("Confirm");
		confirm.setBounds(340, 400, 150, 30);
		confirm.setActionCommand("confirm");
		confirm.addActionListener((ActionListener) frame);
		confirm.setContentAreaFilled(true);
		panel.add(confirm);
	}

	public void defineLogOutButton() {
		logOut = new JButton("Log Out");
		logOut.setBounds(533, 650, 150, 30);
		logOut.setActionCommand("logout");
		logOut.addActionListener((ActionListener) frame);
		logOut.setContentAreaFilled(true);
		panel.add(logOut);
	}

	public void defineUserNameInputField() {
		userName = new JTextField(15);
		userName.setBounds(340, 300, 150, 20);
		panel.add(userName);
	}

	public void definePassWordInputField() {
		passWord = new JPasswordField(15);
		passWord.setBounds(340, 330, 150, 20);
		panel.add(passWord);
	}

	public void defineConfirmPassWordInputField() {
		confirmPassWord = new JPasswordField(15);
		confirmPassWord.setBounds(340, 360, 150, 20);
		panel.add(confirmPassWord);
	}

	public void defineUserNameLabelField() {
		userNameLabel = new JLabel("User Name");
		userNameLabel.setBounds(230, 295, 150, 30);
		panel.add(userNameLabel);
	}

	public void definePassWordLabelField() {
		passWordLabel = new JLabel("PassWord");
		passWordLabel.setBounds(230, 325, 150, 30);
		panel.add(passWordLabel);
	}

	public void defineConfirmPassWordLabelField() {
		confirmPassWordLabel = new JLabel("Confirm PassWord");
		confirmPassWordLabel.setBounds(230, 355, 150, 30);
		panel.add(confirmPassWordLabel);
	}

	public void setAllVisiable(int type) {
		boolean[] set = new boolean[5];
		Arrays.fill(set, false);
		set[type] = true;
		signIn.setVisible(set[1]);
		signUp.setVisible(set[1]);
		back.setVisible(set[2] | set[3]);
		confirm.setVisible(set[2] | set[3]);
		logOut.setVisible(set[4]);

		userName.setVisible(set[2] | set[3]);
		passWord.setVisible(set[2] | set[3]);
		confirmPassWord.setVisible(set[3]);

		userNameLabel.setVisible(set[2] | set[3]);
		passWordLabel.setVisible(set[2] | set[3]);
		confirmPassWordLabel.setVisible(set[3]);
	}

	public String getUserName() {
		return userName.getText();
	}

	@SuppressWarnings("deprecation")
	public String getPassWord() {
		return passWord.getText();
	}

	@SuppressWarnings("deprecation")
	public String getConfirmPassWord() {
		return confirmPassWord.getText();
	}

	public void warningMessage(String message) {
		JOptionPane.showMessageDialog(panel, message, "Warning",
				JOptionPane.WARNING_MESSAGE);
	}

	public void setAllEmpty() {
		userName.setText("");
		passWord.setText("");
		confirmPassWord.setText("");
	}
}

package ClientApp;

import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import ChatServer.*;

public class RoomView {
	public JFrame frame;
	public JPanel panel;
	public JList<String> roomsList, userRoomsList;
	public List<IRoom> rooms;
	public JButton askJoinRoomButton, openRoomButton, openUserRoomButton,
			removeRoomButton, addUserButton, sendMessageButton,
			showMessageButton, createRoomButton, blockUserButton,
			removeUserButton;
	public JScrollPane roomPane, userRoomPane;
	public JLabel userRoomLabel;
	public String[] roomsName, userRoomsName;
	public int indRoom = 0, indUserRoom = 0;

	public RoomView(JFrame mainFrame, JPanel mainPanel) {
		panel = mainPanel;
		frame = mainFrame;
		roomsList = new JList<String>();
		userRoomsList = new JList<String>();
		defineRoomPane();
		defineUserRoomPane();

		defineAskJoinRoomButton();
		defineOpenRoomButton();
		defineOpenUserRoomButton();
		defineRemoveRoomButton();
		defineAddUserButton();

		defineSendMessageButton();
		defineShowMessagesButton();
		defineCreateRoomButton();
		defineBlockUserButton();
		defineRemoveUserButton();

		defineUserRoomLabel();
	}

	public void defineRoomPane() {
		roomPane = new JScrollPane(roomsList);
		roomPane.setBounds(0, 0, 685, 310);
		panel.add(roomPane);
	}

	public void defineUserRoomPane() {
		userRoomPane = new JScrollPane(userRoomsList);
		userRoomPane.setBounds(0, 330, 685, 315);
		panel.add(userRoomPane);
	}

	public void defineAskJoinRoomButton() {
		askJoinRoomButton = new JButton("Ask Join");
		askJoinRoomButton.setBounds(483, 279, 100, 30);
		askJoinRoomButton.setActionCommand("askjoin");
		askJoinRoomButton.addActionListener((ActionListener) frame);
		askJoinRoomButton.setContentAreaFilled(true);
		panel.add(askJoinRoomButton);
	}

	public void defineOpenRoomButton() {
		openRoomButton = new JButton("Open");
		openRoomButton.setBounds(583, 279, 100, 30);
		openRoomButton.setActionCommand("openroom");
		openRoomButton.addActionListener((ActionListener) frame);
		openRoomButton.setContentAreaFilled(true);
		panel.add(openRoomButton);
	}

	public void defineOpenUserRoomButton() {
		openUserRoomButton = new JButton("Open");
		openUserRoomButton.setBounds(583, 613, 100, 30);
		openUserRoomButton.setActionCommand("openuserroom");
		openUserRoomButton.addActionListener((ActionListener) frame);
		openUserRoomButton.setContentAreaFilled(true);
		panel.add(openUserRoomButton);
	}

	public void defineRemoveRoomButton() {
		removeRoomButton = new JButton("Remove");
		removeRoomButton.setBounds(483, 613, 100, 30);
		removeRoomButton.setActionCommand("removeroom");
		removeRoomButton.addActionListener((ActionListener) frame);
		removeRoomButton.setContentAreaFilled(true);
		panel.add(removeRoomButton);
	}

	public void defineAddUserButton() {
		addUserButton = new JButton("Add User");
		addUserButton.setBounds(383, 613, 100, 30);
		addUserButton.setActionCommand("adduser");
		addUserButton.addActionListener((ActionListener) frame);
		addUserButton.setContentAreaFilled(true);
		panel.add(addUserButton);
	}

	public void defineSendMessageButton() {
		sendMessageButton = new JButton("Send Message");
		sendMessageButton.setBounds(413, 650, 120, 30);
		sendMessageButton.setActionCommand("sendmessage");
		sendMessageButton.addActionListener((ActionListener) frame);
		sendMessageButton.setContentAreaFilled(true);
		panel.add(sendMessageButton);
	}

	public void defineShowMessagesButton() {
		showMessageButton = new JButton("Show Message");
		showMessageButton.setBounds(283, 650, 130, 30);
		showMessageButton.setActionCommand("showmessage");
		showMessageButton.addActionListener((ActionListener) frame);
		showMessageButton.setContentAreaFilled(true);
		panel.add(showMessageButton);
	}

	public void defineCreateRoomButton() {
		createRoomButton = new JButton("Create Room");
		createRoomButton.setBounds(163, 650, 120, 30);
		createRoomButton.setActionCommand("createroom");
		createRoomButton.addActionListener((ActionListener) frame);
		createRoomButton.setContentAreaFilled(true);
		panel.add(createRoomButton);
	}

	public void defineBlockUserButton() {
		blockUserButton = new JButton("Block User");
		blockUserButton.setBounds(43, 650, 120, 30);
		blockUserButton.setActionCommand("blockuser");
		blockUserButton.addActionListener((ActionListener) frame);
		blockUserButton.setContentAreaFilled(true);
		panel.add(blockUserButton);
	}

	public void defineRemoveUserButton() {
		removeUserButton = new JButton("Remove User");
		removeUserButton.setBounds(43, 650, 120, 30);
		removeUserButton.setActionCommand("removeuser");
		removeUserButton.addActionListener((ActionListener) frame);
		removeUserButton.setContentAreaFilled(true);
		panel.add(removeUserButton);
	}

	public void defineUserRoomLabel() {
		userRoomLabel = new JLabel("Your Rooms.");
		userRoomLabel.setBounds(1, 310, 150, 20);
		panel.add(userRoomLabel);
	}

	public IRoom getSelectedRoom() {
		if (indRoom == 0)
			return null;
		String selectedRoomName = roomsName[roomsList.getSelectedIndex()];
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getTitle().equals(selectedRoomName))
				return rooms.get(i);
		}
		return null;
	}

	public IRoom getSelectedUserRoom() {
		if (indUserRoom == 0)
			return null;
		String selectedRoomName = userRoomsName[userRoomsList
				.getSelectedIndex()];
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getTitle().equals(selectedRoomName))
				return rooms.get(i);
		}
		return null;
	}

	public boolean member(IRoom room, String userName) {
		List<IUser> userInRoom = room.getUsers();
		for (int i = 0; i < userInRoom.size(); ++i) {
			if (userInRoom.get(i).getName().equals(userName))
				return true;
		}
		return false;
	}

	public void UpdateRoomList(List<IRoom> rooms, IUser userLogIn) {
		this.rooms = rooms;
		roomsName = new String[rooms.size()];
		userRoomsName = new String[rooms.size()];
		indRoom = 0;
		indUserRoom = 0;
		for (int i = 0; i < rooms.size(); ++i) {
			if (userLogIn instanceof AdminUser
					|| member(rooms.get(i), userLogIn.getName()))
				userRoomsName[indUserRoom++] = rooms.get(i).getTitle();
			else
				roomsName[indRoom++] = rooms.get(i).getTitle();
		}
		roomsList = new JList<String>(roomsName);
		roomsList.setSelectedIndex(0);
		defineRoomPane();

		userRoomsList = new JList<String>(userRoomsName);
		userRoomsList.setSelectedIndex(0);
		defineUserRoomPane();
	}

	public void setAllVisiable(int type) {
		boolean[] set = new boolean[5];
		Arrays.fill(set, false);
		set[type] = true;
		roomPane.setVisible(set[1] | set[2]);
		userRoomPane.setVisible(set[1] | set[2]);

		askJoinRoomButton.setVisible(set[1] | set[2]);
		openRoomButton.setVisible(set[1] | set[2]);
		openUserRoomButton.setVisible(set[1] | set[2]);
		removeRoomButton.setVisible(set[1] | set[2]);
		addUserButton.setVisible(set[1] | set[2]);
		sendMessageButton.setVisible(set[1] | set[2]);
		showMessageButton.setVisible(set[1] | set[2]);
		createRoomButton.setVisible(set[1] | set[2]);
		blockUserButton.setVisible(set[1]);
		removeUserButton.setVisible(set[2]);

		userRoomLabel.setVisible(set[1] | set[2]);
	}

}

package PersistanceLayer;

import java.util.*;

import ExceptionLayer.*;
import ChatServer.*;

public interface IPersistanceMechanism {

	public void addUser(int userID, AbstractUser user) throws SQLException;

	public AbstractUser getUser() throws SQLException;

	public void deleteUser(int userID) throws SQLException;

	public void addRoom(int userID, AbstractRoom room) throws SQLException;

	public AbstractRoom getRoom();

	public void deleteRoom(int roomID) throws SQLException;

	public List<AbstractRoom> getAllRooms() throws SQLException;

	public List<AbstractUser> getAllUsers(int roomID);

	public IPersistanceMechanism getInstance() throws SQLException,
			ClassNotFoundException;

	public void deleteUserFromRoom(int roomID, int userID) throws SQLException;
}

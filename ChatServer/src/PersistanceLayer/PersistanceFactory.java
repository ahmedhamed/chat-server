package PersistanceLayer;

import ExceptionLayer.SQLException;

public class PersistanceFactory {
	public IPersistanceMechanism loadPersistanceMechanism(
			String persistanceMechanism) {
		if (persistanceMechanism.equals("SQL"))
			try {
				return new SQLPersistance().getInstance();
			} catch (SQLException e) {
				e.getMessage();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return new FilePersistance().getInstance();
	}
}

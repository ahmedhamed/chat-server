package PersistanceLayer;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import ExceptionLayer.*;
import ChatServer.*;

public class SQLPersistance implements IPersistanceMechanism {

	public SQLPersistance instance;
	public Connection con;
	public Statement stmt;
	public ResultSet rs;

	public void addUser(int userID, AbstractUser user) throws SQLException {
		try {
			PreparedStatement ptmt = con
					.prepareStatement("INSERT INTO [User] ([Name], [PassWord], [UserID], [BlockUserID], [Permission]) VALUES(?,?,?,?,null)");
			ptmt.setString(1, user.getName());
			ptmt.setString(2, user.getPassWord());
			ptmt.setString(3, String.valueOf(userID));
			if (user instanceof GeneralUser) {
				List<IUser> blockedUsers = ((GeneralUser) user)
						.getBlockedUsers();
				if (blockedUsers.size() == 0)
					ptmt.setString(4, "0");
				else {
					int blockedID = blockedUsers.get(blockedUsers.size() - 1)
							.getID();
					ptmt.setString(4, String.valueOf(blockedID));
				}
			} else
				ptmt.setString(4, "0");
			if (user instanceof AdminUser)
				ptmt.setString(5, ((AdminUser) user).getPermissions().get(0));
			ptmt.executeUpdate();
			ptmt.close();
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Can't Inserting In DataBase.");
			throw error;
		}
	}

	public AbstractUser getUser() throws SQLException {
		if (rs != null) {
			try {
				while (rs.next()) {
					String permission = rs.getString("Permission");
					if (permission == null) {
						GeneralUser user = new GeneralUser();
						user.setID(rs.getInt("UserID"));
						user.setName(rs.getString("Name"));
						user.setPassWord(rs.getString("PassWord"));
						int blockUserID = rs.getInt("BlockUserID");
						if (blockUserID != 0) {
							IUser blockedUser = new AbstractUser();
							blockedUser.setID(blockUserID);
							user.addBlockedUser(blockedUser);
						}
						return user;
					} else {
						AdminUser user = new AdminUser();
						user.setID(rs.getInt("UserID"));
						user.setName(rs.getString("Name"));
						user.setPassWord(rs.getString("PassWord"));
						user.addPermission(permission);
						return user;
					}
				}
			} catch (java.sql.SQLException e) {
				SQLException error = new SQLException();
				error.setMessage("Error Loading From DataBase.");
				throw error;
			}
		}
		return null;
	}

	public void deleteUser(int userID) throws SQLException {
		try {
			stmt = con.createStatement();
			stmt.execute("DELETE FROM User WHERE UserID = "
					+ String.valueOf(userID));
			stmt.close();
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Can't Delete This User.");
			throw error;
		}
	}

	public void addRoom(int userID, AbstractRoom room) throws SQLException {
		try {
			PreparedStatement ptmt = con
					.prepareStatement("INSERT INTO [Room] ([Title], [Desc], [RoomID], [UserID]) VALUES(?,?,?,?)");
			ptmt.setString(1, room.getTitle());
			ptmt.setString(2, room.getDesciption());
			ptmt.setString(3, String.valueOf(room.getID()));
			ptmt.setString(4, String.valueOf(userID));
			ptmt.executeUpdate();
			ptmt.close();
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Can't Insert This Room In DataBase.");
			throw error;
		}
	}

	public AbstractRoom getRoom() {
		return null;
	}

	public void deleteRoom(int roomID) throws SQLException {
		try {
			stmt = con.createStatement();
			stmt.execute("DELETE FROM room WHERE RoomID = "
					+ String.valueOf(roomID));
			stmt.close();
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Can't Delete This Room From DataBase.");
			throw error;
		}
	}

	private AbstractRoom found(List<AbstractRoom> rooms, int ID) {
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getID() == ID)
				return rooms.get(i);
		}
		return null;
	}

	public List<AbstractRoom> getAllRooms() throws SQLException {
		List<AbstractRoom> rooms = new ArrayList<AbstractRoom>();
		try {
			stmt = con.createStatement();
			stmt.execute("select * from Room");
			rs = stmt.getResultSet();
			if (rs != null) {
				while (rs.next()) {
					int ID = rs.getInt("RoomID");
					String desc = rs.getString("Desc");
					String title = rs.getString("Title");
					AbstractUser userInRoom = new AbstractUser();
					userInRoom.setID(rs.getInt("UserID"));
					if (desc.equals("general")) {
						GeneralRoom room = (GeneralRoom) found(rooms, ID);
						if (room == null) {
							room = new GeneralRoom(title, desc);
							room.setID(ID);
							room.users.add(userInRoom);
							rooms.add(room);
						} else
							room.users.add(userInRoom);
					} else {
						RestrictedRoom room = (RestrictedRoom) found(rooms, ID);
						List<IUser> allowUsers = new ArrayList<IUser>();
						if (room == null) {
							allowUsers.add(userInRoom);
							room = new RestrictedRoom(title, desc, allowUsers);
							room.setID(ID);
							rooms.add(room);
						} else
							room.users.add(userInRoom);
					}
				}
			}
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Error Loading From DataBase.");
			throw error;
		}
		return rooms;
	}

	public List<AbstractUser> getAllUsers(int roomID) {
		return null;
	}

	public IPersistanceMechanism getInstance() throws SQLException,
			ClassNotFoundException {
		try {
			instance = new SQLPersistance();
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			String accessFileName = "C:\\Users\\HAMED\\Desktop\\SW2\\ChatServer\\src\\PersistanceLayer\\DataBase";
			String connURL = "jdbc:odbc:DRIVER={Microsoft Access Driver (*.mdb)};DBQ="
					+ accessFileName + ".mdb;";
			instance.con = DriverManager.getConnection(connURL, "", "");
			instance.stmt = instance.con.createStatement();
			instance.stmt.execute("select * from User");
			instance.rs = (instance.stmt).getResultSet();
		} catch (ClassNotFoundException e) {
			SQLException error = new SQLException();
			error.setMessage("Error Class Not Found.");
			throw error;
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Error Loading DataBase.");
			throw error;
		}
		return instance;
	}

	public void deleteUserFromRoom(int roomID, int userID) throws SQLException {
		try {
			stmt = con.createStatement();
			stmt.execute("DELETE FROM room WHERE RoomID = "
					+ String.valueOf(roomID) + " AND UserID = "
					+ String.valueOf(userID));
			stmt.close();
		} catch (java.sql.SQLException e) {
			SQLException error = new SQLException();
			error.setMessage("Error Delete This User From DataBase.");
			throw error;
		}
	}
}

package ExceptionLayer;

public class PersistenceException extends Exception {

	public static final long serialVersionUID = 1L;
	public String msg;

	public void setMessage(String msg) {
		this.msg = msg;
	}

	public String getMessage() {
		return msg;
	}

}

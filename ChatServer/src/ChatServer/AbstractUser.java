package ChatServer;


public class AbstractUser implements IUser {
	public int id;
	public String name, passWord;

	public void setID(int userID) {
		id = userID;
	}

	public int getID() {
		return id;
	}

	public void setName(String userName) {
		name = userName;
	}

	public String getName() {
		return name;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getPassWord() {
		return passWord;
	}
}

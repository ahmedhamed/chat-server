package ChatServer;

public interface IUser {
	public void setID(int userID);

	public int getID();

	public void setName(String userName);

	public String getName();

	public void setPassWord(String passWord);

	public String getPassWord();
}

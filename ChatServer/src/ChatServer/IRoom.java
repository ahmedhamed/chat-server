package ChatServer;

import java.util.*;

public interface IRoom {
	public void setTitle(String title);

	public String getTitle();

	public void setDecription(String desc);

	public String getDesciption();

	public void addUser(IUser user);

	public List<IUser> getUsers();

	public void setID(int roomId);

	public int getID();

}

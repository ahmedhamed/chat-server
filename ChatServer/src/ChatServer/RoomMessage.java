package ChatServer;

import java.util.*;

public class RoomMessage extends ChatMessage {
	public List<IRoom> roomList;

	public RoomMessage(IUser sender, List<IRoom> roomList, String message) {
		send = sender;
		this.roomList = roomList;
		this.message = message;
	}

	public RoomMessage(IUser sender, IRoom room, String message) {
		roomList = new ArrayList<IRoom>();
		send = sender;
		roomList.add(room);
		this.message = message;
	}
}

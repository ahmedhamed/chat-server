package ChatServer;

import java.util.*;

public class GeneralUser extends AbstractUser {
	public List<IUser> blockedUser;

	public GeneralUser() {
		blockedUser = new ArrayList<IUser>();
	}

	public void addBlockedUser(IUser blockedUser) {
		this.blockedUser.add(blockedUser);
	}

	public List<IUser> getBlockedUsers() {
		return blockedUser;
	}

}

package ChatServer;

import java.util.*;

public class AbstractRoom implements IRoom {
	public String title;
	public String desciption;
	public List<IUser> users;
	public int id;

	public AbstractRoom() {
		users = new ArrayList<IUser>();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setDecription(String desc) {
		desciption = desc;
	}

	public String getDesciption() {
		return desciption;
	}

	public void addUser(IUser user) {
		users.add(user);
	}

	public List<IUser> getUsers() {
		return users;
	}

	public void setID(int roomId) {
		id = roomId;
	}

	public int getID() {
		return id;
	}
}

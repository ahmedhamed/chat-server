package ChatServer;

import java.util.*;

public interface IChatServer {
	public void addUser(GeneralUser user);

	public void addRoomAdmin(AdminUser admin, List<String> permissions);

	public void removeUser(int userID);

	public void removeAllUser(int roomID);

	public void removeRoom(int roomID);

	public List<IRoom> getRooms();

	public List<IUser> getUsers();

	public void joinRoom(int roomID, int userID);

	public void removeAllRooms();

	public void creatRestirctedRoom(String title, String desc,
			List<IUser> allowdUsers);

	public void createGenralRoom(String title, String desc, AbstractUser owner);

	public void sendMessage(ChatMessage message);

	public void leaveRoom(int roomID, int userID);

	public ChatServer getInstance();

	public void loadPersistanceConfigurations();

	public void blockUser(int userID, int blockedUserID);
}

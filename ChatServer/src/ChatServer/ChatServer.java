package ChatServer;

import java.util.*;

import javax.swing.JOptionPane;

import ExceptionLayer.*;
import PersistanceLayer.*;

public class ChatServer implements IChatServer {

	public List<IRoom> rooms;
	public List<IUser> users;
	public ChatServer chatServer;
	public IPersistanceMechanism persistanceMechanism;

	public void addUser(GeneralUser user) {
		int lastID = 0;
		boolean alreadyExit = false;
		for (int i = 0; i < users.size(); ++i) {
			if (users.get(i).getName().equals(user.getName())) {
				alreadyExit = true;
				break;
			}
			if (lastID < users.get(i).getID())
				lastID = users.get(i).getID();
		}
		if (alreadyExit)
			return;
		user.setID(++lastID);
		try {
			persistanceMechanism.addUser(lastID, user);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		users.add(user);
	}

	public void addRoomAdmin(AdminUser admin, List<String> permissions) {
		boolean exit = false;
		for (int i = 0; i < users.size(); ++i) {
			if (users.get(i) instanceof AdminUser) {
				AdminUser isAdmin = (AdminUser) users.get(i);
				if (isAdmin.getID() == admin.getID()) {
					((AdminUser) users.get(i))
							.addPermission(permissions.get(0));
					exit = true;
				}
			}
		}
		if (!exit) {
			admin.permissions = permissions;
			users.add(admin);
		}
	}

	public void removeUser(int userID) {
		try {
			persistanceMechanism.deleteUser(userID);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		for (int i = 0; i < users.size(); ++i) {
			if (users.get(i).getID() == userID) {
				users.remove(i);
				return;
			}
		}
	}

	public void removeAllUser(int roomID) {
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getID() == roomID) {
				IRoom tempRoom = new AbstractRoom();
				tempRoom.setID(rooms.get(i).getID());
				tempRoom.setTitle(rooms.get(i).getTitle());
				tempRoom.setDecription(rooms.get(i).getDesciption());
				rooms.remove(i);
				rooms.add(tempRoom);
				return;
			}
		}
	}

	public void removeRoom(int roomID) {
		try {
			persistanceMechanism.deleteRoom(roomID);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getID() == roomID) {
				rooms.remove(i);
				return;
			}
		}
	}

	public List<IRoom> getRooms() {
		return rooms;
	}

	public List<IUser> getUsers() {
		return users;
	}

	public void joinRoom(int roomID, int userID) {
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getID() == roomID) {
				try {
					persistanceMechanism.addRoom(userID,
							(AbstractRoom) rooms.get(i));
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),
							"Warning", JOptionPane.WARNING_MESSAGE);
					return;
				}
				for (int j = 0; j < users.size(); ++j) {
					if (users.get(j).getID() == userID)
						rooms.get(i).addUser((AbstractUser) users.get(j));
				}
			}
		}
	}

	public void removeAllRooms() {
		rooms.clear();
	}

	public void creatRestirctedRoom(String title, String desc,
			List<IUser> allowdUsers) {
		int lastID = 0;
		for (int i = 0; i < rooms.size(); ++i) {
			if (lastID < rooms.get(i).getID())
				lastID = rooms.get(i).getID();
		}
		RestrictedRoom newRestrictedRoom = new RestrictedRoom(title, desc,
				allowdUsers);
		newRestrictedRoom.setID(++lastID);
		rooms.add(newRestrictedRoom);
		try {
			persistanceMechanism.addRoom(allowdUsers.get(0).getID(),
					newRestrictedRoom);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
	}

	public void createGenralRoom(String title, String desc, AbstractUser owner) {
		int lastID = 0;
		for (int i = 0; i < rooms.size(); ++i) {
			if (lastID < rooms.get(i).getID())
				lastID = rooms.get(i).getID();
		}
		GeneralRoom newGenralRoom = new GeneralRoom(title, desc);
		newGenralRoom.setID(++lastID);
		newGenralRoom.addUser(owner);
		rooms.add(newGenralRoom);
		try {
			persistanceMechanism.addRoom(owner.getID(), newGenralRoom);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
	}

	public void sendMessage(ChatMessage message) {
	}

	public void leaveRoom(int roomID, int userID) {
		for (int i = 0; i < rooms.size(); ++i) {
			if (rooms.get(i).getID() == roomID) {
				List<IUser> usersInRoom = rooms.get(i).getUsers();
				for (int j = 0; j < usersInRoom.size(); ++j)
					if (usersInRoom.get(j).getID() == userID) {
						try {
							persistanceMechanism.deleteUserFromRoom(roomID,
									userID);
						} catch (SQLException e) {
							JOptionPane.showMessageDialog(null, e.getMessage(),
									"Warning", JOptionPane.WARNING_MESSAGE);
							return;
						}
						loadPersistanceConfigurations();
						return;
					}
			}
		}
	}

	public ChatServer getInstance() {
		chatServer = new ChatServer();
		chatServer.rooms = new ArrayList<IRoom>();
		chatServer.users = new ArrayList<IUser>();
		PersistanceFactory persistanceFactory = new PersistanceFactory();
		chatServer.persistanceMechanism = persistanceFactory
				.loadPersistanceMechanism("SQL");
		return chatServer;
	}

	public void loadPersistanceConfigurations() {
		if (users.size() == 0) {
			while (true) {
				AbstractUser user = null;
				try {
					user = persistanceMechanism.getUser();
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage(),
							"Warning", JOptionPane.WARNING_MESSAGE);
					return;
				}
				if (user == null)
					break;
				if (user instanceof AdminUser) {
					AdminUser isAdmin = (AdminUser) user;
					addRoomAdmin(isAdmin, isAdmin.getPermissions());
				} else {
					GeneralUser isGeneral = (GeneralUser) user;
					boolean exit = false;
					for (int i = 0; i < users.size(); ++i) {
						if (users.get(i).getID() == isGeneral.getID()) {
							exit = true;
							int blockUserID = isGeneral.getBlockedUsers()
									.get(0).getID();
							for (int j = 0; j < users.size(); ++j) {
								if (users.get(j).getID() == blockUserID)
									((GeneralUser) users.get(i))
											.addBlockedUser(users.get(j));
							}
						}
					}
					if (!exit) {
						if (isGeneral.getBlockedUsers().size() != 0) {
							int blockUserID = isGeneral.getBlockedUsers()
									.get(0).getID();
							isGeneral.blockedUser.clear();
							if (blockUserID != 0) {
								for (int j = 0; j < users.size(); ++j) {
									if (users.get(j).getID() == blockUserID)
										isGeneral.addBlockedUser(users.get(j));
								}
							}
						}
						users.add(isGeneral);
					}
				}
			}
		}
		rooms = new ArrayList<IRoom>();
		List<AbstractRoom> room = null;
		try {
			room = persistanceMechanism.getAllRooms();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		for (int i = 0; i < room.size(); ++i) {
			rooms.add(room.get(i));
			for (int j = 0; j < rooms.get(i).getUsers().size(); ++j) {
				IUser userInRoom = rooms.get(i).getUsers().get(j);
				for (int k = 0; k < users.size(); ++k) {
					if (userInRoom.getID() == users.get(k).getID()) {
						userInRoom.setName(users.get(k).getName());
						userInRoom.setPassWord(users.get(k).getPassWord());
					}
				}
			}
		}
	}

	public void blockUser(int userID, int blockedUserID) {
		for (int i = 0; i < users.size(); ++i) {
			if (users.get(i).getID() == userID) {
				if (users.get(i) instanceof GeneralUser) {
					GeneralUser tempUser = (GeneralUser) users.get(i);
					for (int j = 0; j < users.size(); ++j) {
						if (users.get(j).getID() == blockedUserID) {
							tempUser.addBlockedUser(users.get(j));
							try {
								persistanceMechanism.addUser(userID, tempUser);
							} catch (SQLException e) {
								JOptionPane.showMessageDialog(null,
										e.getMessage(), "Warning",
										JOptionPane.WARNING_MESSAGE);
								return;
							}
						}
					}
				}
			}
		}
	}
}

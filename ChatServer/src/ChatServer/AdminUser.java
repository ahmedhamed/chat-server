package ChatServer;

import java.util.*;

public class AdminUser extends AbstractUser {
	public List<String> permissions;

	public AdminUser() {
		permissions = new ArrayList<String>();
	}

	public void addPermission(String permission) {
		permissions.add(permission);
	}

	public List<String> getPermissions() {
		return permissions;
	}
}

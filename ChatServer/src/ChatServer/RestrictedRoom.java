package ChatServer;

import java.util.*;

public class RestrictedRoom extends AbstractRoom {

	public RestrictedRoom(String title, String desc, List<IUser> allowedUsers) {
		this.title = title;
		desciption = desc;
		users = allowedUsers;
	}
}

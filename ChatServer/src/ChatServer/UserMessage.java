package ChatServer;

import java.util.*;

public class UserMessage extends ChatMessage {

	public List<IUser> recipients;

	public UserMessage(IUser sender, IUser recipient, String message) {
		send = sender;
		recipients = new ArrayList<IUser>();
		recipients.add(recipient);
		this.message = message;
	}

	public UserMessage(IUser sender, List<IUser> recipientsList, String message) {
		send = sender;
		this.message = message;
		recipients = recipientsList;
	}
}
